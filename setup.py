import sys
from setuptools import setup, find_packages

version = '1.0.12'

install_requires = [
    'cryptography>=1.5.2',
    'requests>=2.11.1',
    'coloredlogs>=9.0',
    'setuptools>=39.0'
]

if sys.version_info < (3,):
    # install_requires.append('scapy-ssl_tls')
    pass
else:
    # install_requires.append('scapy-python3')
    pass

setup(
    name='foxy_tcpproxy',
    version=version,
    packages=find_packages(),
    include_package_data=True,
    install_requires=install_requires,
    url='http://cloudfoxy.com',
    long_description=open('README.md').read(),
    license=open('LICENSE').read(),
    author='Enigma Bridge Ltd, Smart Arcs Ltd',
    author_email='support@smartarchitects.co.uk',
    description='TCP proxy for Cloud Foxy - cloud platform for smart cards',
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Developers',
        'Programming Language :: Python',
        'Programming Language :: Python :: 2',
        'Programming Language :: Python :: 2.6',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Topic :: Internet :: WWW/HTTP',
        'Topic :: Security',
    ],
    extras_require={
        # 'dev': dev_extras,
        # 'docs': docs_extras,
    },

    entry_points={
        'console_scripts': [
            'foxy_tcpproxy=foxy_tcpproxy:main'
        ],
    }
)
